---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Model
    # Desc of page, used for SEO, start of sentence super important
    description: How to use models in the ORM.
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"


#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---

<a name="models"></a>
# Models

- [Models](#models)
  - [Relationships](#relationships)
  - [Using a Model](#using)
    - [Creating](#creating)
    - [Setting](#setting)
    - [Getting](#getting)
    - [Pushing, Adding](#pushing-adding)
    - [Multiple Updates](#multiple-update)
    - [Saving](#saving)
    - [Converting](#converting)
  - [Finding](#finding)
    - [Find](#find)
    - [Find One](#findOne)
    - [Find By ID](#findByID)
  - [Deleting](#deleting)


A model is a programmatic representation of data in your database. In Nuclio, you define a Model as a class with properties which represent fields in a table/collection.

The ORM Model is a special class which you can extend to define your own models. Doc block annotations are used to define table/collection names, property data types, and property limits.

#### Example:

```
use nuclio\plugin\database\orm\Model;

/**
 * @Collection Test
 */
class Test extends Model
{
  /**
   * @Id
   */
  public mixed $id=null;

  /**
   * @Type string
   */
  public ?string $foo=null;

  /**
   * @Type int
   */
  public ?int $bar=null;

  /**
   * @Type timestamp
   */
  public ?string $datetime=null;

  /**
   * @Type int
   * @Length 1
   * @Default 1
   */
  public ?int $status=null;
}
```

<a name="relationships"></a>
### Relationships

Quite often you will want Models to relate to each other. You can use the `@Relate` annotation.

#### Example:

```
use nuclio\plugin\database\orm\Model;
use nuclio\plugin\user\model\User;

/**
 * @Collection profile
 */
class Profile extends Model
{
  /**
   * @Id
   */
  public mixed $id=null;

  /**
   * @Relate[nuclio\plugin\user\model\User,id]
   */
  public ?User $user=null;

  /**
   * @Type string
   */
  public ?string $firstName=null;

  /**
   * @Type int
   */
  public ?int $lastName=null;
}
```

Nuclio will manage the relationship automagically. However if you're running against a database like MySQL, you will need to use the command line tool to generate the tables and indexes.

```
hhvm vendor/bin/nuclio db::build --config <application>/config/
```

This command will create a file named `models.map` inside of the `.nuclio` folder. It contains meta data required for the `ORM` and `QueryBuilder` to work.

<a name="using"></a>
### Using a Model

<a name="creating"></a>
#### Creating

First, lets look at how a model is initialized.


```
$profileModel=Profile::create();
```

That's the simplest way. But you can also initialize it with its property values already set.

```
$profileModel=Profile::create
(
  Map
  {
    'firstName'=>'Test',
    'lastName'=>'User'
  }
);
```

You can also assign values after the object has been created.

```
$profileModel=Profile::create();
$profileModel->setFirstName('Test');
$profileModel->setLastName('User');
```

Notice we've used methods which represent setters for the properties. These are magic methods which are dynamically available on a model. There are various prefixes which can be used, each one does something different.

<a name="setting"></a>
#### Setting:

Sets a value.

```
$profileModel->setFirstName('Test');
```

<a name="getting"></a>
#### Getting:
Gets a value.

```
$profileModel->getFirstName();
```


<a name="pushing-adding"></a>
#### Pushing, Adding

Adds a value to an array of values.

```
$profileModel->addEmail('test@example.com');
$profileModel->pushEmail('test@example.com');
```

<a name="multiple-update"></a>
### Multiple Updates

You can pass a `Map` of key value pairs to set multiple values at once.

```
$profileModel->setMany
(
  Map
  {
    'firstName'=>'Test',
    'lastName'=>'User'
  }
);
```

<a name="saving"></a>
#### Saving

When manipulating values in a model, they are not automatically written to the database.

To save the model, call the `save` method.

```
$profileModel=Profile::create();
$profileModel->setFirstName('Test');
$profileModel->setLastName('User');
$profileModel->save();
```

<a name="converting"></a>
#### Converting

You can convert a Model to a `Map` or `Array`.

```
var_dump($profileModel->toMap());
var_dump($profileModel->toArray())
```

<a name="finding"></a>
### Finding

Nuclio comes several useful ways to fetch models from a database.

<a name="find"></a>
#### Find

Finds all matching records and returns a cursor.

When iterating through the cursor, the cursor returns model instances of the record.

```
$resultCursor=Profile::find(Map{'name'=>'Test'});
foreach ($resultCursor as $profileModel)
{
  //Do stuff with $profileModel
}
```

<a name="findOne"></a>
#### Find One

Finds and returns the first matching record.

```
$profileModel=Profile::findOne(Map{'name'=>'Test'});
```

<a name="findByID"></a>
#### Find By ID

Finds and returns a record which matches the given ID.

```
$profileModel=Profile::findById(1152);
```

<a name="deleting"></a>
### Deleting

A model can be deleted. But not a model instance.

```
Profile::delete(Map{'name'=>'Test'});
```
