---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Documentation Homepage
    # Desc of page, used for SEO, start of sentence super important
    description: 
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 3

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# Table of Contents

##Prerequisites

- [Release Notes](./release-notes/index.html)
- [Release Methodology](./release-methodology/index.html)
- [Contributing](./contributing/index.html)
- [System Requirements](./system-requirements/index.html)


##Getting Started

- [Installation](./installing/index.html)
- [Configuration](./configuration/index.html)
- [App Directory Structure](./directory-structure/index.html)
- [Error Handling](./error-handling/index.html)
- [Logging](./logging/index.html)
- [Development Environments](./dev-environments/index.html)

##Core Concepts

-[Requests](./requests/index.html)
-[Routing](./routing/index.html)
-[Controllers](./controllers/index.html)
-[Database](./database/index.html)

##Views & Templates
-[Views](./views/index.html)
-[Templates](./templates/index.html)

##General Topics	

-[Session Handling](./sessions/index.html)
-[Validation](./validation/index.html)
-[Cache](./cache/index.html)	
-[File Storage](./file-storage/index.html)
-[EMail](./email/index.html)
-[Class Manager](./class-manager/index.html)
-[Collections](./collections/index.html)

























