---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Configuring Nuclio
    # Desc of page, used for SEO, start of sentence super important
    description: How to configure the Nuclio Framework
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Configuration

- [Introduction](#introduction)
	- [Quick Tips](#quick-tips)
	- [Using Configuration](#accessing)
  - [Application Configuration](#appConfig)


<a name=“introduction”></a>
##Introduction
All of the configuration files for Nuclio are stored in the `<application>/config` directory.  You may want to have a look at NEON and YAML as Nuclio supports these two types of config:

 - [NE-ON](http://ne-on.org/)
 - [YAML](http://www.yaml.org/start.html)
 
 <a name=“quick-tips”></a>
###Quick Tips
You can start developing right away as Nuclio needs only simple configuration. You can go through the config file inside `<application>/config` and modify the options which are specific to your application.

<a name=“app”></a>
#### Configurating Files - sampleApp

If you've generated a sample application, you'll already have some config setup for you.

You can generate a sample application like so:

`$ hhvm vendor/bin/atomos make::app --name MyFirstApp --sample`

Generating an application with sample code will give you a config folder with `application.neon` and `database.neon` config files.

`application.neon` contains most of the application specific configuration.

Below is an example of what that file looks like:
```
    application:
    	name: MyFirstApp
    	mode: development
    	debug: true

    controller:
    	twig:
    		debug: true
    		cache: false
    		viewCacheDir: .nuclio/viewCache/
    		templateDir: .nuclio/template/

    auth:
    	dataSource: primary

    user:
    	dataSource: primary

    session:
    	type: database
    	driver: primary

    router:
    	autoRoute: false

```
As you can see, each section of config is broken up into sections. `application`, `controller`, `auth` etc.

Nuclio allows you to break up your config into multiple files. For example, you could create a config file named `controller.neon` and move the controller config into it like so:
```
    controller:
      twig:
        debug: true
        cache: false
        viewCacheDir: .nuclio/viewCache/
        templateDir: .nuclio/template/

```
Most plugins require some configuration. Each of those plugins contain documentation on how to configure and use them.

<a name=“accessing”></a>
###Using Configuration

Nuclio comes with a config plugin. You can load config like this:

```
$config=Config::getInstance($configPath);
```

The path is the directory where Nuclio will locate all your config file. The files will be ingested and managed by the config plugin instance.

* You can fetch all config like this:
```
$config->getConfig();
```
* Fetching a specific config value:
```
$config->get('application.debug');
```

<a name="appConfig"></a>

###Application Configuration

If you're using Nuclio's Application plugin (recommended), then the config has already been loaded for you. You can access it from within the application like so:

```
$this->getConfig()->get('application.debug');
```

When initializing your application you can specify the directory where the application will load the config from.
```
MySampleApp::getInstance('mySampleApp/config/');
```

This is very useful because you can share config between multiple applications.
```
$sampleApp=MySampleApp::getInstance('config/');
$otherApp=SomeOtherApp::getInstance('config/');

$sampleApp->init();
$otherApp->init();
```
