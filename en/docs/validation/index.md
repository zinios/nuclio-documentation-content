---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Sanitation and Validation
    # Desc of page, used for SEO, start of sentence super important
    description: How to sanitise and validate user inputs
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Sanitation & Validation

- [Introduction](#intro)
- [Sanitation](#sanitation)
- [Validation](#validation)
	- [General Functions](#general-functions)


<a name="intro"></a>
You should validate and sanitise input data to make sure that the data is safe and in the format you expect. Hacklang uses the equivalent of the PHP `filter` extension, which provides a comprehensive way to sanitise user input.

This extension filters data by either validating or sanitising it. This is especially useful when the data source contains unknown (or foreign) data, like user supplied input. For example, this data may come from an `HTML` form.

There are two main types of filtering: `validation` and `sanitation`.

 - `Validation` is used to validate or check if the data meets certain qualifications. For example, passing in `FILTER_VALIDATE_EMAIL` will determine if the data is a valid email address, but will not change the data itself.
 - `Sanitation` will sanitise the data, so it may alter it by removing undesired characters. For example, passing in `FILTER_SANITIZE_EMAIL` will remove characters that are inappropriate for an email address to contain. However, do remember it does not validate the data.

Flags are optionally used with both validation and sanitation to tweak behaviour according to need. For example, passing in `FILTER_FLAG_PATH_REQUIRED` while filtering an URL will require a path (like /foo in http://example.org/foo) to be present.

<a name="sanitation"></a>
##Sanitation
Sanitation involves ensuring that the submitted data is free from unexpected content. So if you expect a number, we need to make sure the submitted data **is** a number. You can also convert user data into other types. Everything submitted is initially treated like a string, so forcing known-numeric data into being an integer or float makes sanitation fast and painless.

An example of this is:
 - `FILTER_SANITIZE_NUMBER_INT` will remove all characters except digits, plus and minus sign.
 - `FILTER_SANITIZE_EMAIL` will remove all characters except letters, digits and !#$%&'*+-=?^_{|}~@.[].
 - `FILTER_SANITIZE_NUMBER_FLOAT` will remove all characters except digits, +- and optionally .,eE.


<a href=validation> </a>
##Validation
Once the input data is sanitised, it is important to make sure that the data submitted contains values you can actually work with.

If you are expecting a number between 1 and 10, you need to check that value. Likewise, if the data came from what should be a drop-down menu, make sure that the submitted value is one which appeared in the menu.

<a href=general-functions> </a>
###General Functions

The following functions can be used with the validation class:

- `isNum`

This checks if the  data is a number type.

- `LenChr`

This checks if the string length is more or less than a specific value.

- `WhiSpc`

This checks if there is a white space in the incoming string.

- `isEmail`

This checks if the incoming string is in Email format.

- `isURL`

This checks if the incoming string is in URL format.

- `isMatch`

This checks if both incoming strings match.
