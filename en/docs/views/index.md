---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Views
    # Desc of page, used for SEO, start of sentence super important
    description: How to implement views in Nuclio 
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Views

Views contain the HTML served by your application, and act as a convenient method of separating your controller and domain logic from your presentation logic. Views are stored in the `./app/view/` directory.

View may look like this:
```
//index.twig.tpl
<html>
    <body>
        <p>Nuclio</>
    </body>
</html>
```
You can render the view into the browser with this:
```
//Sample of rendering view file.
$view=ViewController::getInstance();
$view->render('index.twig.tpl');
```
###How to Pass Data to View

You can pass data to view using the `setKeyVal` or `setKeyVals` before rendering the view:
```
//Sample of passing the data into the view.
$view=ViewController::getInstance();
$view->setKeyVal('key','val');
$view->render('index.twig.tpl');
```
Passing data as an array
```
//Sample of passing multiple data into the view.
$view=ViewController::getInstance();
$view->setKeyVals(['key'=>'val','key2'=>'val2']);
$view->render('index.twig.tpl');
```