---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Database
    # Desc of page, used for SEO, start of sentence super important
    description: How to configure, connect and query datasources such as MongoDB and MySQL
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"


#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---

<a name="databases"></a>
# Databases

- [Databases](#databases)
  - [Configuration](#configuration)
  - [Data Sources & Management](#data-source-and-management)
  - [Database Drivers](#database-drivers)
    - [The Common Interface](#common-interface)
    - [Direct Database Access](#direct-access)




Nuclio makes connecting with databases very straightforward.


<a name="configuration"></a>
## Configuration

Your standard database configuration for your application can be found in `<application>/config/database.neon`. This is where all of your database connections will be defined.

```
database:
	connections: {
  	primary: {
			driver: mysql,
			dbname: nuclio,
			host: localhost,
			username: root,
			password: root
		},
  	secondary: {
			driver: mongo,
			host: localhost,
			dbname: test
		}
  }
```

The above is an example of a database config file. Note that you can have more than one connection.

It is recommended to name your main default connection `primary`, but you don't have to.

<a name="data-source-and-management"></a>
## Data Sources & Management

Data sources are created and used to access data from a database.

The Data Source Manager is responsible creating connections as sources. The manager will hold the sources and they can be accessed from anywhere in the codebase by their connection names.

#### Example:

```
use nuclio\plugin\database\datasource\manager\Manager as DataSourceManager;

$primaryConfig=$config->get('database.connections.primary');
$manager=DataSourceManager::getInstance();

$manager->createConnection('primary',new Map($primaryConfig));
```

You can later fetch the connection like this:

```
$source=$manager->getSource('primary');
```

<a name="database-drivers"></a>
## Database Drivers

The Nuclio Framework supports the following database servers:

  - MongoDB
  - MySQL (PDO Based)

We are working hard to add more in the very near future.

There are two ways to work with a database driver. We'll have a look at both in this document.

<a name="common-interface"></a>
### The Common Interface

Nuclio provides a set of `Common Interface` classes for accessing database functions.

The interfaces are used to create compatibility between different types of database servers. Ultimately this allows you to switch between databases without rewriting code.

The first interface, `nuclio\plugin\database\common\CommonInterface`, contains basic transactional method definitions.

It contains the following method definitions:

* beginTransaction
* commit
* rollBack

The main interface which is used by database drivers is `nuclio\plugin\database\common\CommonQueryInterface`. It contains all the querying methods.

It contains the following method definitions:

* find
* findOne
* findById
* upsert
* delete
* distinct
* listCollections
* listCollectionFields
* buildColumn
* collectionExists
* columnExists

The last interface is `nuclio\plugin\database\common\CommonCursorInterface`. This interface is used by driver specific cursors.

Cursors are returned by database drivers rather than full record sets. This is done for optimization purposes, to keep the memory footprint of your application low.

The cursor interface is an extension of the PHP SPL's `Iterator` interface and works exactly the same way with the addition of a few extra methods to help with cursor filtering.

It contains the following method definitions:

* current
* key
* next
* rewind
* valid
* seek
* count
* limit
* offset
* orderBy

<a name="direct-access"></a>
### Direct Database Access

Of course there are times where a common interface between database functionality is not possible. This is often times due to unique features in one database over another. Nuclio doesn't stop you from accessing this functionality, and provides a way for you to access those underlying features by directly accessing the database driver.

You do this by fetching the driver instance through the data source.

#### Example:

```
$driver=$manager->getSource('primary')->getDriver();
```

> Note:
> In order to pass the Hack type checker, you'll need to perform an additional check on the driver object *before* using it.
> This is because `getDriver` returns an object which implements `nuclio\plugin\database\common\CommonInterface`. This makes sense since the source cannot know the driver class which it is holding (it's dynamic).

Example using MongoDB:
```
use nuclio\plugin\database\driver\mongo\Mongo;

$driver=$manager->getSource('primary')->getDriver();

if ($driver instanceof Mongo)
{
  $result=$driver->aggregate
  (
    'user',
    Vector{...}
  );

  //...
}
```
