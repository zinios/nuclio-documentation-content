---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Controllers
    # Desc of page, used for SEO, start of sentence super important
    description: How Controllers are used within Nuclio
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# Controllers

 - [The Controller](#controller)
 - [Jigsaw Controller](#jigsaw)
	- [Passable](#passable-traits)
	- [Viewable](#viewable-traits)
	- [Restful](#restful-traits)
	- [Routable](#reoutable-traits)


 <a name="controller"></a>   
## The Controller

The `Controller` is one of the key components of the Model-View-Controller pattern. As the name suggests, it is responsible for controlling the flow of the application execution. When you make a request (e.g. request a page) to an MVC application, a `controller` is responsible for returning the response to that request. A `controller` can have one or more actions and has the ability to return different types of action results to one particular request.

The `controller` acts as the coordinator between the `View` and the `Model`. The `Controller` receives input from users via the `View`, then processes the user's data with the help of the `Model` passing the results back to the `View`.

<a name="jigsaw"></a>
## Jigsaw Controllers

Nuclio provides a vanilla controller with all the very basic utilities for bootstrapping application logic but Nuclio's real strength comes from controller traits.

These traits allow you to control the behaviour of your controllers to be as specific as you want them to be.

A controller can take the role of one or many of the "MVC" components.

Nuclio uses four different types of traits - `Passable`, `Viewable`, `Restful` & `Routable`. These are described below:

> Note: Any class which uses any of the controller traits must be extended from the `Controller` class.

<a name="passable-traits"></a>
###Passable

The Passable trait enables you to pass control to another `viewable` controller. When passing control to a `viewable` controller, you can control which method is executed. Additionally, data can be passed to it. This enables you to setup the view to be "dumb", eliminating business logic from the view.

An example of this is:

```
namespace sampleApp\controller
{
  use nuclio\plugin\application\controller\Controller;
  use nuclio\plugin\application\controller\Passable;

  class Index extends Controller
  {
  	use Passable;

    public function _index():void
		{
      print $this->passControlTo
			(
				'sampleApp\\controller\\view\\IndexView',
				'run',
				Map
				{
					'content'		=>'home.twig.tpl',
					'authenticated'	=>Auth::getInstance()->isAuthenticated()
				}
			);
		}
  }
}
```

 <a name="viewable-traits"></a>   	
###Viewable

The `Viewable` trait allows your controller to be able to load and manipulate templates and then return that template to a parent controller.

Controllers which are purely viewable, should not contain business logic. They are dumb controllers which work with the information given to them by their parent controllers (via `Passable::passControlTo`)

An example of this is:

```
namespace sampleApp\controller\view
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Viewable;

	class IndexView extends Controller
	{
		use Viewable;

		public function run(Map<string,mixed> $data):string
		{
			$this->setMany($data);
			return $this->render('index.twig.tpl');
		}
	}
}
```


<a name="restful-traits"></a>
###Restful

The `Restful` trait gives a controller the ability to accept and respond to RESTful requests. This is very useful for the creation of APIs.

An example of this is:

```
namespace sampleApp\controller\api
{
	use sampleApp\SampleApp;
	use nuclio\plugin\
	{
		application\controller\Controller,
		application\controller\Restful,
		auth\Auth,
		auth\AuthException,
		session\Session
	};

	class Example extends Controller
	{
		use Restful;

		public function login():void
		{
			$auth=Auth::getInstance();

			//If already authenticated, ignore this login request.
			if ($auth->isAuthenticated())
			{
				$this->setResponseCode(200);
				$this->respond(true,'Already logged in');
			}

			//Pull out the post information from the request handler.
			$request	=$this->getRequest();
			$email		=$request->getPost('email');
			$password	=$request->getPost('password');

			try
			{
				/*
					Try to authenticate the user. An exception will
					be thrown if the request fails.
				 */
				$auth->authenticate($email,$password);
				$this->setResponseCode(200);
				$this->respond(true,'Success');
			}
			catch (AuthException $exception)
			{
				$this->setResponseCode(200);
				$this->respond(false,'Invalid login');
			}
		}
	}
}
```



<a name="routable-traits"></a>   
###Routable Trait

A `Routable` trait has the ability to pass full control of a request to another controller.

Nuclio encourages developers to separate the logic of their applications into as many small controllers as possible, which increases readability and maintainability. To succeed with this goal, Nuclio provides the `Routable` trait, which enables a developer to do just that.

Imagine your application as a series of containers. Each container is a bundle of logic, views and data. When a page is first loaded, you want all relevant containers to be loaded but what controls the relevance? A parent routable controller.

When the `routable` controller is executed, it's job is to initialize other controllers, passing valuable information to them so that those child controllers can complete their jobs. At the same time, the parent controller instructs the child controller NOT to output, but rather return the response.

The parent controller is then responsible for using the responses from each child component and outputting them in a way that makes sense.

Each child controller must return a Map, containing at least a `view` but the controller may return more information than that.

Let's look at an example of this:

```
namespace sampleApp\controller
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Routable;
	use nuclio\plugin\http\uri\URI;

	class ContainerRouter extends Controller
	{
		use Routable;
		use Passable;

		public function run():void
		{
			$contentPage		=$this->getURI()->getPart(1);
			$headResponse		=$this->routeControlTo('sampleApp\\controller\\container\\DefaultPage','head');
			$headerResponse	=$this->routeControlTo('sampleApp\\controller\\container\\DefaultPage','header');
			$contentResponse=$this->routeControlTo('sampleApp\\controller\\container\\DefaultPage','content',Map{'page'=>$contentPage});
			$footerResponse	=$this->routeControlTo('sampleApp\\controller\\container\\DefaultPage','footer');

			print $this->passControlTo
			(
				'sampleApp\\controller\\view\\DefaultPageView',
				'structure',
				Map
				{
					'head'		=>$headResponse->get('view'),
					'header'	=>$headerResponse->get('view'),
					'content'	=>$contentResponse->get('view'),
					'footer'	=>$footerResponse->get('view')
				}
			);
		}
	}
}
```

By constructing an application like this, you not only end up with a cleaner and more maintainable code, but you also end up with a stronger and more dynamic application. This allows you to setup `routes` as API calls to those child controllers. So for example, you could refresh parts of your frontend without the need for a complete page refresh.

An important thing to note about child controllers is that they are `instructed` to NOT return by parent controllers. It is up to the developer to code in the instruction correctly. Take a look at the example below:

```
namespace sampleApp\controller\container
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Routable;

	class DefaultPage extends Controller
	{
		use Routable;

		public function header(bool $canOutput=true,Map<string,mixed> $data):Map<string,mixed>
		{
			$viewResult=$this->routeControlTo('workbench\\controller\\view\\DefaultPageView','header');
			if ($canOutput)
			{
				print $viewResult->get('view');
			}
			return $viewResult;
		}
	}
}

```

The `$canOutput` option needs to be used in the method to determine if the method should print the output or not.

Note that the controller methods should always return a Map with the signature `Map<string,mixed>`.
