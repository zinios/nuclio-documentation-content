---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Contributing to Nuclio
    # Desc of page, used for SEO, start of sentence super important
    description: How to help Nuclio grow and develop
    # subtitle of the page
    subtitle: There's lots of ways contribute to the Nuclio community
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# Contributing to Nuclio

- [Reporting an Issue](#bugs)
    - [Collaboration](#discussion)
    - [Security Issues](#security)
- [Coding Standard](#coding)
    - [Nuclio DocBlock](#doc)


<a name="bugs"></a>

## Reporting an Issue

We believe community collaboration is key to developing the very best software and solutions, and we strongly encourage you to submit pull requests and bug fixes as and when they arise.

In order for us to fix a bug, we need to be able to reproduce it. Please note that all bug reports should contain a clear description of the issue so we can easily follow your actions and recreate the same situation. Please include as much relevant information as possible, as well as a code sample that demonstrates the issue. The more information you can provide, the sooner your issue can be fixed. 

Before submitting your issue, please check to see if anyone has already reported it. Don't forget to search for all issues, not just open ones, as there's a chance that your bug might have already been reported and closed. 

The Nuclio source code is managed on Bitbucket, and there are dedicated repositories for each of the Nuclio projects:

- [Nuclio Framework](https://bitbucket.org/zinios/nuclio-standard)
- [Nuclio Documentation](https://bitbucket.org/zinios/nuclio-documentation-content)


<a name="discussion"></a>

## Collaboration

We welcome all suggestions for new features and improvements to the Nuclio platform. 
If you wish to propose a new feature, please be willing to help implement at least some of the code that would be needed to see that feature completed. 

To propose a new Nuclio feature please send an email directly to support at <a href="mailto:support@nuclio.co">support@nuclio.co</a>. 

<a name="security"></a>

## Security Issues

Should you discover a security vulnerability or critical issue within Nuclio, please send an email directly to support at <a href="mailto:support@nuclio.co">support@nuclio.co</a>. 

<a name="coding"></a>

## Coding Standard

Nuclio loosely follows the [PSR-4](https://github.com/php-fig/fig-standards/tree/master/accepted) coding standard. The main difference being that we use **tabs** instead of **spaces** for indentation.

<a name="doc"></a>

### Nuclio DocBlock example

Below is an example of a valid Nuclio documentation block. Note that the `@param` attribute is followed by two spaces, the argument type, two more spaces, and finally the variable name:
```
    /**
     * Register a binding with the container.
     *
     * @param  string|array  $abstract
     * @param  \Closure|string|null  $concrete
     * @param  bool  $shared
     * @return void
     */
    public function bind($abstract, $concrete = null, $shared = false)
    {
        //
    }
```