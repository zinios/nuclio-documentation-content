---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Templates
    # Desc of page, used for SEO, start of sentence super important
    description: How to use the Nuclio template engine
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Templates

- [What is a template?](#what)
- [Twig](#twig)
	- [Render](#render)
	- [Change Path](#addpath)

<a name="what"></a> 
##What is a template?
A template is simply a text file. It can generate any text-based format (HTML, XML, CSV, LaTeX, etc.). It doesn't have a specific extension, .html or .xml are acceptable.

A template contains variables or expressions, which get replaced with values when the template is evaluated; and tags, which control the logic of the template.

<a name="twig"></a> 
###Twig
The Nuclio Framework uses a template engine called Twig. Twig compiles templates down to plain optimized PHP code. We use Twig because it is fast, secure and flexible.

> Note: The recommended way to install Twig is using `composer`. The installation guide for Twig can be found [here.](http://twig.sensiolabs.org/doc/installation.html)

The example below demonstrates some basic features of Twig:
```
{% extends "base.html" %}
{% block navigation %}
    <ul id="navigation">
    {% for item in navigation %}
        <li>
            <a href="{{ item.href }}">
                {% if 2 == item.level %}&nbsp;&nbsp;{% endif %}
                {{ item.caption|upper }}
            </a>
        </li>
    {% endfor %}
    </ul>
{% endblock navigation %}
```
<a name="render"></a> 
####Render
You may want to render a TWIG template.
``` 
//Passing the template which you want to be rendered.
$twig=Twig::getInstance();
$twig->render($template)
```
<a name="addpath"></a> 
####Change Path
You may want to set another path instead of `view` folder to render the template.
```
//Encode array to NEON formatted string
$twig=Twig::getInstance();
$twig->addPath($path, $namespace)
```