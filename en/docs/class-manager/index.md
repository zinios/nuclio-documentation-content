---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Class Manager
    # Desc of page, used for SEO, start of sentence super important
    description: The Class Manager provides an easy way to invoke instances of Nuclio classes.
    # subtitle of the page
    subtitle: Create classes from the class manager
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---

# Class Manager
- [Introduction](#introduction)
- [Class Compatibility](#compatibility)
- [Singletons](#singletons)
- [Factories](#factories)
- [Containers](#containers)

<a name="introduction"> </a>

The class manager is Nuclio's class registry. It handles initialization of classes and maintains singletons and factories.

<a name="compatibility"> </a>
## Class Compatibility

In order for your class to be compatible with the Class Manager, you need to specify a few things on your class.

Lets take a look at a typical class.

```
class Foo
{

}
```

Class Foo does nothing. We initialize it the traditional way:

```
$foo=new Foo();
```

This won't use the Class Manager, as there is no way the class manager can know of the new class, unless you were to manually register it with the Class Manager.

In order to use the class manager, we need to create a static method, and initialize the class statically.

```
use nuclio\core\ClassManager;

class Foo
{
  public static function getInstance(/* HH_FIXME[4033] */ ...$args):Foo
  {
  	$instance=ClassManager::getClassInstance(self::class,...$args);
  	return ($instance instanceof self)?$instance:new self(...$args);
  }
}

$foo=Foo::getInstance();
```

The `getInstance` method is a **type safe** way of in initializing a class dynamically with the Class Manager.

Note that Hack's type checker doesn't yet support `...` so by adding `/* HH_FIXME[4033] */` the type checker ignores it and won't generate an error.

Class attributes can also be applied in order to change it's behavior when being initialized.

<a name="singletons"> </a>
## Singletons

A singleton means that there can only ever be one instance of a class.

By default, all classes that are initialized through the Class Manager are singletons.

You can still specify, for readability sake, that a class is singleton by setting it as a class attribute.

```
use nuclio\core\ClassManager;

<<singleton>>
class Foo
{
  public static function getInstance(/* HH_FIXME[4033] */ ...$args):Foo
  {
  	$instance=ClassManager::getClassInstance(self::class,...$args);
  	return ($instance instanceof self)?$instance:new self(...$args);
  }
}

$foo=Foo::getInstance();
```

<a name="factories"> </a>
## Factories

A factory means that there can be one or more instances of a class.

Specify a class as a factory by setting it as a class attribute.

```
use nuclio\core\ClassManager;

<<factory>>
class Foo
{
  public static function getInstance(/* HH_FIXME[4033] */ ...$args):Foo
  {
  	$instance=ClassManager::getClassInstance(self::class,...$args);
  	return ($instance instanceof self)?$instance:new self(...$args);
  }
}

$foo=Foo::getInstance();
$foo2=Foo::getInstance();
```

<a name="containers"> </a>
## Containers

Sometimes a class needs to be a singleton, in the context of an application or plugin, and must be unique in that context. But the application or plugin is a factory. As the class is a singleton, this causes the class to be shared in unexpected ways.

By declaring a singleton as also a container, the Class Manager will create a single instance of the class based on a given container name.

In order for it to be usable, your class needs to declare a different static method.

```
use nuclio\core\ClassManager;

<<
  singleton,
  container
>>
class Foo
{
  public static function getInstance(/* HH_FIXME[4033] */ ...$args):Foo
  {
  	$instance=ClassManager::getClassInstance(self::class,...$args);
  	return ($instance instanceof self)?$instance:new self(...$args);
  }

  public static function getContainerInstance(string $container, /* HH_FIXME[4033] */ ...$args):Foo
  {
  	$instance=ClassManager::getContainerClassInstance($container,self::class,...$args);
  	return ($instance instanceof self)?$instance:new self(...$args);
  }
}

$foo=Foo::getContainerInstance('MyContainer1');
$foo2=Foo::getContainerInstance('MyContainer2');
```
