---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: ORM
    # Desc of page, used for SEO, start of sentence super important
    description: How to use the ORM.
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"


#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---

<a name="orm"></a>
# ORM

- [ORM](#orm)
  - [Setup](#setup)
  - [Models](#models)
	- [Dynamic Models](#dynamicModels)
	- [Finding Models](#finding)



<a name="setup"></a>
## Setup

The ORM must be bound with a datasource before it is used.

```
$ORM=ORM::getInstance();
$ORM->bindDataSource(DataSourceManager::getSource('primary'));
```

<a name="models"></a>
## Models
There's more information about models [here](../database-models/index.html)

<a name="dynamicModels"></a>
## Dynamic Models

Sometimes you cannot define a model or you need to work with an existing model. Nuclio provides the `DynamicModel` class for this purpose.

It has all the features of a `Model` with the added bonus of being able to save to any collection/table and set any property.

#### Example:

```
$dynamicCollection=DynamicModel::create('dynamicCollection');
$dynamicCollection->someNewField('foo');
$dynamicCollection->someOtherField('bar');
$dynamicCollection->save();
```


<a name="finding"></a>
## Finding Models

Sometimes you can't work with a model directly to find records.

Note that these methods will return Models as `DyanmicModel` instances.
### Find

Finds all matching records and returns a cursor.

When iterating through the cursor, the cursor returns model instances of the record.

```
$resultCursor=ORM::find('profile',Map{'name'=>'Test'});
foreach ($resultCursor as $profileModel)
{
  //Do stuff with $profileModel
}
```

### Find One

Finds and returns the first matching record.

```
$profileModel=ORM::findOne('profile',Map{'name'=>'Test'});
```

### Find By ID

Finds and returns a record which matches the given ID.

```
$profileModel=ORM::findById('profile',1152);
```
