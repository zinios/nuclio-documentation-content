---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: 1.0 Release Notes
    # Desc of page, used for SEO, start of sentence super important
    description: The release notes for v1.0 of the Nuclio Framework
    # subtitle of the page
    subtitle: The release notes cover new features, bug fixes and any backward compatibility breaks
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Release Notes
## Introduction
The Nuclio Framework is made up of a common core, and many plugins, [released periodically](../release-methodology/index.html). For each release, we will list the combined
release notes on this page. For each of the core files and plugins you can find individual change logs 
in each of their respective directories (see [Directory Structure](../directory-structure/index.html) for locations)

---

## Releases

- [Nuclio 1.0](#nuclio-1.0)

---
<a name="nuclio-1.0"></a>
### Nuclio 1.0

Nuclio 1.0 introduces our HackLang application framework. Every feature and function is new and 
provides a strong base for building web-based applications. As everything is new, everything is a
change! So, it seems silly to list them out here...

Browse the topics in the documentation to get an idea of the flexibility and power of Nuclio.
