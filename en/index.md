---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Documentation Homepage
    # Desc of page, used for SEO, start of sentence super important
    description: 
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 2
#The version of nuclio that this document refers to
nuclioVersion: "1.0"

layout: default
sidebar:
    version: 1
    #Force the sidebar to link to a specific version. YOU MUST INCLUDE A TRAILING SLASH /
    #versionForce: "1.0/"

language:
    collection: 1
    current: en

---
# Nuclio Framework v1.0
Welcome to the first release of the Nuclio Framework. We hope you find it useful.

Nuclio 1.0 is a LTS release.

<!--- Don't mess with this bit
-->
$$div class='note js-note blue-light'$!
$$div class='note-header'$!
Documentation Subject To Frequent Changes!
$$/div$!
$$p class='note-description'$!
Nuclio is still a work in progress [(want to help?)](./docs/contributing/index.html) and we will be
continuing to increase and improve the documentation over the coming weeks and months.
Be sure to check back often to get the updates.
$$/p$!
$$/div$!

You can also keep up to date by cloning the documentation repo [(HowTo)](#clonedocs)


---
We have  documentation to help you


####[Developer Documentation](./docs/index.html)
Developer Documentation introduces the Nuclio Framework, how to install and configure
it, and key concepts you need to know to leverage it's power and flexibility.



***
## Documentation for older versions of the Nuclio Framework
This is the first release of the Nuclio Framework, so there aren't any older 
versions of the documentation available.



***
<a name="clonedocs"></a>
## Local installs
### Documentation Repo
The Nuclio documentation is held in it's own repository, and has been designed to
be cloned and viewed off-line, locally on your development machine.

    git clone git@bitbucket.org:zinios/co.nuclio.docs.git

The repo contains a flat-file HTML version of the documentation website.

Once cloned, point your browser at the `./public/index.html` to view the documentation locally.