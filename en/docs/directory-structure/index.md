---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Directory Structure
    # Desc of page, used for SEO, start of sentence super important
    description: Where the different components of the Nuclio Framework are stored
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# Directory Structure

- [Introduction](#intro)
- [The Root Directory](#initial)
	- [The .nuclio Directory](#nuclio)
	- [The public Directory](#public)
	- [Recommended Application Structure](#app)


<a name="intro"></a>

## Introduction

The default Nuclio directory structure is the ideal starting block for building your applications. When additional apps are added, the directories will appear on this list.


<a name="inital"></a>

## The Root Directory

Once you install the Nuclio development environment for the first time, preferably using the [Vagrant[(../dev-environment-vagrant/index.html) installation, you will end up with the following bare bones structure ready for you to start developing. The files can be located in `/srv/www/nuclio.dev.lan/`

The project file structure looks like this:

```
.
├── composer.json
├── composer.lock
├── .hhconfig
├── init.hh
├── .nuclio
├── public
│   └── index.hh
└── vendor
    ├── ...
```


<a name="nuclio"></a>
###The .nuclio Directory

The `.nuclio` directory is Nuclio's temp directory. Depending on what plugins or apps you're using, it may contain more or some of these files and folders.

```
├── .nuclio
│    ├── actions.map
│    ├── cache
│    ├── log
│    └── providers.map
```

<a name="public"></a>
###The public Directory

The `public` directory contains the `index.hh` file, which is the entry point for all requests entering your application.

<a name="app"></a>
###Recommended Application Structure

Below is an example application which uses the recommended application layout.

This directory contains:
```
├── ExampleApp
│   ├── config
│   │   ├── application.neon
│   │   ├── database.neon
│   │   └── policy.neon
│   ├── controller
│   │   ├── api
│   │   │   └── ...
│   │   ├── view
│   │   │   └── ...
│   │   └── ...
│   ├── ExampleApp.hh
│   └── template
│       └── ...
```
