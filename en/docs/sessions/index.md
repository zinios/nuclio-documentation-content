---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Session Management
    # Desc of page, used for SEO, start of sentence super important
    description: How to implement session management with Nuclio
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Session Handling

- [Sessions](#sessions) 	
	- [Local Sessions](#local) 	
	- [Database Sessions](#database) 	
	- [Session Functions](#session-functions)


<a name="sessions"></a>  	
### Sessions
Sessions enable you to store the state of the users with some information regarding login time and some security checks related to authentication and permissions. The Nuclio Framework provides two type of sessions;

- File session
- Database session

The file session uses the server's local files to save the user's sessions whereas the database session stores the user's session within the database.

Choosing between different types of sessions can be done by the `config`.
<a name="local"></a>  
####For local file sessions:

	    session:
			type: storage
			driver: fileSystem::local-disk
<a name="database"></a>  
####For database sessions:

	    session:
			type: database
			driver: primary

<a name="session-functions"></a>  
#### Session Functions:

-  getInstance

This creates a new `source` instance and returns it. Internally, this will call the constructor.

- get

This provides a key for data supposed to be stored with the session data.

- set

This updates or adds new key with it's value to the session.

- delete

This deletes or set the field added to the session to null.

- sessionId()

This returns the session ID as string.

- getSessionName

This grabs the session name (id) from the cookie. Unless something has gone wrong, the session name in the cookie should always be the same as the session id.

- start

This starts a new session.

- isExpired

This checks if the session has expired.
