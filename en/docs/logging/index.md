---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Logging
    # Desc of page, used for SEO, start of sentence super important
    description: How to use logging within the Nuclio Framework
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Logging 

- [Introduction](#intro)
- [Configuration](#config)
- [Logger](#logger)
	- [Logger Functions](#functions)
- [Logger Drivers](#drivers)

<a name="intro"></a>  
Logging is an important part of the app development/maintenance cycle. It’s not just about the data you log, but also about how you do it. We use a Nuclio plugin called `Logger` for all logging. 

<a name="config"></a>   
##Configuration

Nuclio supports several ways of logging using drivers which can be configured in the `config` files. The `config` files can also be used to configure timezone which allows localisation of your logs.

````
logger:
    channel: {
            logger: testlog,
            driver: monolog,
            timeZone: Asia/Singapore,
            path: /var/log/nuclio
    }
````
 <a name="logger"></a>   
## Logger
The `logger` class is used as the main interface which will redirect the called functions to the configured driver.

By default, Nuclio is configured to create a log file for your application in the `.nuclio/logs` directory. 

<a name="functions"></a>   
###Logger Functions:

* Construct
You may write information to the logs using the following constructor:

````
public function __construct(Map< string,string> $options)
{
     parent::__construct();
    $channel = $options->get('channel');
    $log = $channel['logger'];
    $driverName = $channel['driver'];
    if (is_null($driverName) || !is_string($driverName))
    {
        throw new LoggerException('Unable to load driver for logger. Driver must be provided as a string.');
    }
    $this->driverName=$driverName;
    $driverIntance=ProviderManager::request('logger::'.$this->driverName,$options);
    if ($driverIntance instanceof loggerCommonInterface)
    {
        $this->driver=$driverIntance;
    }
    else
    {
        throw new LoggerException(sprintf('Unable to load driver for logger. Driver "%s" is not available.',$this->driverName));
    }
}
````

The logger provides the eight logging levels defined in RFC 5424: `emergency`, `alert`, `critical`, `error`, `warning`, `notice`, `info` and `message`.

````
public function log(int $level, $message, array<mixed> $context=[]):loggerCommonInterface
{
   return $this->driver->log($message, $context);  // Log a message
   return $this->driver->info($message, $context);  // Log a piece of info or a note.
   return $this->driver->notice($message, $context);  // Log an notice
   return $this->driver->warning($message, $context);  // Log a warning
   return $this->driver->error($message, $context);  // Log an error
   return $this->driver->critical($message, $parameter); // Log a critical event
   return $this->driver->alert($message, $context); // Log an alert
   return $this->driver->emergency($message, $context); // Log an emergency
   return $this->driver->debug($message, $context); // Log a debug message
}
````
 <a name="drivers"></a>   
##Logger Drivers

Currently, Nuclio supports two drivers:

##### *Monolog Logging*
    
Monolog is one of, if not, the best logging libraries out there for PHP. It supports logging to various different handlers. It also allows you to specify which level each handler should log. For example, you may want to log error events to a Slack channel, in order to notify your developers team of a fatal error. Monolog also supports formatters. If you want to format your data as JSON before sending them to a service like Loggly, you can use the `JsonFormatter` class to do the job. 

##### *dbLogger*

For database logging, Nuclio provides flexibility for the user to choose which table to save the logs into. The table name in the database will be named according to the value provided in the **path** field in the config file. Every database logging will add *5 fields* in that table:

1. logger: the title of the log
2. action: type of logging (error, emergency , normal logging ...etc)
3. info: any additional message to be added to the log
4. dateTime: add the date and time for the log (this is done automatically)
5. context
 