---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Routing
    # Desc of page, used for SEO, start of sentence super important
    description: How to implement URL routing with Nuclio
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# ROUTING

- [Router](#router)
	- [Define new Route](#define-new-route)
	- [Executing a route](#executing-a-route)
	- [Validate a route](#validate-a-route)
	- [Find Matching Routes](#find-matching-routes)

Routing provides you with tools which map URLs to the `controller` actions. By defining routes, you can separate how your application is implemented from how its URLs are structured.

The Nuclio Framework provides two ways of defining the route; either `auto` route or `manual` route. You can set the options inside the `config` file by changing the `autoRoute` to either `true` or `false`.

If you set `autoRoute` to `true`, you don't need to specify the route. It will work as a simple router whereby it will check your URL and route you automatically. Whereas, if you set `autoRoute` to `false`, you must always specify the route to be taken.

```
    $pluginRegRef=$this->getRegistryRef();
    $this->routes['MyRestFunction']=Route::create();
	$this->routes['MyRestFunction']->setRegisteredBy($pluginRegRef);
	$this->routes['MyRestFunction']->setPath('/test/MyRestFunction');
	$this->routes['MyRestFunction']->setAction('test\\controller\\rest\\TestClass@MyRestFunction');
	$this->routes['MyRestFunction']->setMethod('GET');
	$this->routes['MyRestFunction']->save();
```

<a name="router"></a>
### Router
Before creating a new route, you need to initialize the `Router` plugin.
```
//Assign instance of Router plugin.
$router=Router::getInstance();
```
<a name="define-new-route"></a>
#### Define new Route
`newRoute` is used to create a new route.

Below are some examples of defining a new route:

  - Passing URL, method, and function directly into the function call.
    ```
    $router->newRoute
    (
    	'/test/direct',
    	function()
    	{
    		echo 'FUNCTION HERE';
    	},
    	'GET'
    );
    ```
  - Passing the controller and action to be executed on the given URL
    ```
    //example 1
    $router->newRoute('/','controller\\index@_index','GET');

    //example 2
    $router->newRoute('/foo/bar','controller\\test@add','GET');
    ```
  - Passing the URL as a pattern/regex
    ```
    //example 1
    $router->newRoute('/foo/bar/(:any)','controller\\foo@bar','GET');

    //example 2
    $router->newRoute('/foo/bar/(:any)/(:any)','controller\\foo@bar','GET');
    ```
<a name="executing-a-route"></a>
#### Executing a route

`execute` is used to execute the route. This function is used in `Service` base class to load the related route to the service.

```
$router->newRoute('/foo/bar/(:any)','controller\\foo@bar','GET');
try
{
	$this->router->execute();
}
catch (RouterException $exception)
{
  //404 the request.
}
```

<a name="validate-a-route"></a>
#### Validate a route

`canRouteTo` will validate whether the URL is a valid route or not. This method is used in `Service` base class to validate the url before executing the route.

<a name="find-matching-routes"></a>
#### Find Matching Routes

`findMatchingRoutes` is used to get list of matching routes from the database.
