---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Vagrant Development Environments for the Nuclio Framework
    # Desc of page, used for SEO, start of sentence super important
    description: How to install and run the pre-built Vagrant Virtual Machine development environment for the Nuclio Framework.
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Vagrant Development Environment
- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
	- [Other useful 3rd Party applications](#useful)
- [Vagrant Image](#image)
- [Installation Issues](#issues)
- [Using the VM](#using)
- [Protips for better use](#protips)
	- [Using Multiple Vagrants](#multiple)
	- [Saving the State of the machine](#state)
	- [Taking Snapshots](#snapshots)
- [Changing the configuration defaults](#configuration)
	- [Port Forwarding](#port)
	- [Shared Folders](#shared)


<a name="introduction"></a>
##Introduction

Nuclio provides a vagrant install to allow developers a quick and easy installation in order to get them hacking away much quicker. The vagrant install will automatically create a new virtual machine which contains everything you will need to start developing using the Nuclio framework.

It contains:
* Ubuntu 14.04
* HHVM v 3.15.1
* Composer v 1.2.1
* Nuclio Framework v1.0
* MongoDB
* Memcached

<a name="prerequisites"></a>
##Prerequisites

There are a couple of prerequisites you need installed before you can go ahead and install your Nuclio vagrant. These are:

- Vagrant - 1.8.5 or above
- Virtualbox - 5.1.4 or above
- an SSH Client (already installed on Linux and Mac's, but Windows users should use PuTTY)

The installation guides for Vagrant and Virtualbox can be found in their respective websites. Best practice dictates that you should download the latest version of these packages, unless a particular version is recommended. Both applications come in various packages to suit your host operating system. Please ensure you download the correct version for your own system. 

###Virtual Box
VirtualBox is the Virtual Machine controller. It provides the 'simulated' hardware for the Virtual Machines to interact with. You use it to manage the different virtual machines installed on your computer.

[Virtualbox](https://www.virtualbox.org/wiki/Downloads) can be downloaded free from the Oracle website. It comes in various packages to suit your host operating system. 

Please ensure you download the correct version for your own system, and that it's version 5.1.4 or higher.



####Ubuntu Installation
Ubuntu installation is more tricky than expected as the default repositories use the older 5.0 VitualBox.
We've had success following [these instructions.](https://www.linuxbabe.com/virtualbox/install-latest-virtualbox-5-1-ubuntu-16-04)

###Vagrant
Vagrant is a powerful provisioning engine that allows the configuration and installation of Virtual Machine images to be scripted and automated.
It interacts with VirtualBox to download and configure a Virtual Machine Image. By scripting and automating the installation and configuration of the Virtual Machine, you can be sure that each person using the same Vagrant script has the exact same copy of the Virtual Machine.

[Vagrant](https://www.vagrantup.com/downloads.html) can be downloaded free from the Vagrant Up website. 

## 
Once both of the above packages are installed successfully, you are ready to install the Nuclio vagrant.

<a name="useful"></a>
###Other useful 3rd Party applications

There are a few 3rd party software applications that can be downloaded to make development easier. These are:

- a text editor which supports Hack e.g. Atom with Nuclide
- PuTTY Telnet client (for windows users)

If you don't already have these 3rd party apps installed, you can find the installation procedures [here](../3rdparty/).

<a name="image"></a>
##Vagrant Virtual Machine Image

Vagrant installation can be easily done by carrying out the following steps:

1. Open up your terminal window and navigate to where you want to install the Nuclio VM.
1. Use the `makedir` to create a new folder. You are free to name it whatever you like, but in these instructions we use `newfolder`.
1. Change directory into your new folder using the command `cd newfolder`
1. Use the command `git clone https://bitbucket.org/zinios/nuclio-vagrant.git` - The repo is very small and will clone quickly.
1. Once the repository has been cloned successfully, change into the directory created with `cd nuclio-vagrant`. This directory contains your vagrant config file.
1. Type `vagrant up` to start the installation - The first time you do this, Vagrant will download the Nuclio virtual machine image (approx 835Mb), so it make take some time.
1. Once installation is complete, open up the 'virtualbox' application and you will see a new virtual machine called Nuclio-Standard-Edition. 

##Notes:
You can connect to your virtualbox by using the `vagrant ssh` command and check that `/srv/www/nuclio.dev.lan` contains folders and files. 
You should also be able to connect to [http://127.0.0.1:8080](http://127.0.0.1:8080) and see the Nuclio demo application page
You can suspend the virtual machine with `vagrant suspend` and shut it down with `vagrant halt` - see [https://www.vagrantup.com/docs/getting-started/teardown.html](https://www.vagrantup.com/docs/getting-started/teardown.html) for more commands.


<a name="issues"></a>
##Known Installation Issues

Any known issues with installation are detailed below:

###Ubuntu installations

####Interrupted Downloads
The Virtual Machine image that needs to be downloaded is quite large, and we have
observed that occasionally, the connection drops between the Vagrant downloader utilty
and S3 (where the image is stored).
You may see this error:

    An error occurred while downloading the remote file. The error message, if any, is reproduced below. Please fix this error and try again.
    SSL read: error:00000000:lib(0):func(0):reason(0), errno 104

If you run `vagrant up` again, the downloading process will resume from where it left off and continue the installation process.

###Nuclio Vagrant Installations

####CommonInterface Error 
There is currently a known error which appears on the Nuclio vagrant installation script relating to the `CommonInterface.hh` file. This issue has been logged and is currently being investigated. 

####This site can’t be reached
If the HHVM hasn't started and you try to connect to http://127.0.0.1:8080/, your browser may show an error message stating that the site you're connecting to can't be reached. Check the status of the HHVM by logging into the VM (see instructions below)
and using the `sudo service --status-all` command. You should see a `[+]` before the `hhvm` entry.
Otherwise, use the `sudo service hhvm restart` command to restart the service.

<a name="using"></a>
##Using the Virtual Machine

Once installation is complete, you are ready to start working with Nuclio! 


Connect to the Nuclio environment via a SSH client (like PuTTY) using the following connection info:
```
hostname: 127.0.0.1:2222
Connection type: SSH
```
If the connection is successful, you will be asked for your login details. 
```
Nuclio-Vagrant login details are:
login: vagrant
pwd: vagrant
```

<a name="protips"></a>
##Protips for Better Development

<a name="multiple"></a>
###Using Multiple Vagrants

It is possible to install more than one instance of the Nuclio Vagrant in your VirtualBox. To do this, you need to rename the existing instance of the `Nuclio-Standard-Edition` virtual machine. This can be done in either of the following ways:

####From GUI
Using the Virtualbox application:
1. Shut down the virtual machine. Note: the machine must be in a powered off state not a saved one.
2. Right-click on the machine, and navigate to `Settings > General tab > Basic tab` and type in a new name in `Name` box.
3. Click `OK`.
4. Now confirm in your VirtualBox window that the machine has been renamed.

####From the Terminal

1. Shut down the virtual machine to be renamed.
2. In the terminal, navigate to the folder which contains the 'Vboxmanage' app. 
	a. For windows users, it can be found in `C:\Program Files\Oracle\VirtualBox`
	b. For mac users, it can be found in `/Applications/VirtualBox.app/Contents/MacOS/VBoxManage`
3. Type the following command in the terminal: `VBoxManage modifyvm ORIGNAL_NAME --name NEW_NAME`
4. Now confirm in your VirtualBox window that the machine has been renamed.
5. You are now free to run the vagrant install steps again to create an additional machine.

<a name="state"></a>
###Saving the State of the Machine
Using the Vagrant commands, you can `vagrant suspend` and `vagrant halt` to pause and shutdown the virtual machine, respectively.

In the VirtualBox application, when you click the `close` button on the top right of your window you are given the following three choices:

1. Save the machine state - the machine state is frozen and the saved as is to your local disk. The VM will start exactly where you left off when restarted.
2. Send the shutdown signal - this triggers the proper shutdown sequences closing all programs. This is the equivalent of pressing the power button on a real computer.
3. Power off the machine - this cuts the power to the virtual machine and closes it without saving any data. This option is not recommended.

It is important that you understand what the three options mean and make the correct choice each and every time to close your machine.

<a name="snapshots"></a>
###Snapshots

The snapshot function on VirtualBox allows you to save a known-safe state which can be restored at a later date.  This is good, if you need to roll back to a particular state after having made some changes to the system. The difference between take snapshot and the above save machine state is that many snapshots can be taken and stored for future use.

####Taking Snapshots
How you take a snapshot depends on the state of the machine:

* if the VM machine is `running`, you select `Take Snapshot` from the `Machine` pull down menu of the VM window. A window will pop up and ask you for a `snapshot name`. This name is for reference only and is helpful to remind you of the state of the snapshot so be descriptive. You can also add more detail to the `Description` field as well. The new snapshot will be displayed in the snapshots list. 

* if the VM machine is in a `saved` or `powered off` sate, you click on the `Snaphots` tab on the top right of the VM window and select either the small camera icon, or right click on th `current state` item in the list and select`take snapshot`. As above, a window will pop up and ask you for a `snapshot name`. The new snapshot will then be displayed in the snapshots list. 

####Restore a Snapshot
To restore a snapshot, you right-click the snapshot you want which appears on the snapshot list. By restoring a snapshot, you go back (or forward) in time: the current state of the machine is lost, and the machine is restored to the exact state it was in when the snapshot was taken. It is recommended that you take a snapshot of your current state before restoring a previous snapshot.

####Delete a Snapshot

To delete a snapshot, right-click on it in the snapshots tree and select `Delete`. This will not affect the state of the virtual machine, but only release the files on disk that VirtualBox used to store the snapshot data, therefore freeing up disk space.

###Host Key
The `Host Key` setting identifies the key which toggles whether the cursor is controlled by the VM or by the Host operating system window. It can also be used to trigger certain VM actions. The `Host Key` is by default the right Control Key on your keyboard. This setting can be changed via the global settings.


<a name="configuration"></a>
##Changing the Default Configuration

The Nuclio vagrant already has a basic configuration set up for you. If you need to change the preconfigured parameters, you are free to do so. Below are a few examples of the common features you may choose to alter.

<a name="port"></a>
###Port Forwarding

To change the preconfigured ports in the VirtualBox:

1. Right-click on the machine, and navigate to `Settings > Network tab` and click on the advanced tab to expand the options. 
2. Click on the port forwarding  button to open up the Port Forwarding Rules window.
3. The Nuclio vagrant is preconfigured with the following: 
(Host port is the port you connect to from your machine, Guest port is the port 'inside' the VM)

```
SSH
Host port: 2222
Guest Port: 22

HTTP
Host port: 8080
Guest Port: 80

MongoDB
Host port:27019
Guest port:27017
```

<a name="shared"></a>
###Shared Folders

The shared folders access is already preconfigured:
In the VirtualBox application:
1. Right-click on the machine, and navigate to `Settings > Shared Folders tab`, which shows the preconfigured shared folders. 
2. To add a shared folder, just click on the + sign on the right hand side of the box.

> Note: Shared folders have read/write access to the files in the host path by default. 

###Global Settings
In the VirtualBox application:
Global settings are the settings which apply to all virtual machines of the current user or in the case of Extensions to the entire system. These global settings can be accessed via the `File > Preferences`, or alternatively `Ctrl+G`, which will display a pop-up with the following tabs: `General`, `Input`, `Update`, `Language`, `Display`, `Network`, `Extensions` & `Proxy`.

More detailed instructions on how to use the VirtualBox machine can be found [here](https://www.virtualbox.org/manual/).
