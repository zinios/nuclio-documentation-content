---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Request and Response
    # Desc of page, used for SEO, start of sentence super important
    description: How to use Request and Response objects to interact with the outside world.
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# HTTP Requests 

- [Introduction](#introduction)
- [Request Class](#request-class)
	- [Request method](#request-method)
		- [Request Query string Value](#request-query-string-value)
		- [Validate HTTP Request Method](#validate-http-request-method)
		- [POST Request data](#post-request-object)
		- [PUT Request Data](#put-request-data)
		- [HEAD Request Data](#head-request-data)
		- [OPTION Request Data](#option-request-data)
		- [SERVER Request Data](#server-request-data)
		- [COOKIE Request Data](#cookie-request-data)
		- [File Request Data](#file-request-data)
		- [Request Object](#request-object)
		- [Request Body Raw Data](#request-body-raw-data)
		- [Retrieve Response header](#retrieve-response-header)
		- [Retrieve Values of response header](#retrieve-values)
		- [Retrieve Language](#retrieve-language)
		- [Retrieve Response Format](#retrieve-format)
		- [Check if Request is secure](#request-secure)
		- [Check if Request is an Ajax](#request-ajax)
		- [Retrieve Server Name](#retrieve-server-name)
		- [Retrieve HOST Name](#retrieve-host-name)
- [Responses](#reponses)



<a name="introduction"></a>
HTTP is the principal of data communication for the World Wide Web. It functions as a request-reponse protocol in the client-server computing model. To put this in real terms, a web browser would be the client and an application running on a computer which host a website would be the server. The client would send an HTTP request message to the server, which in turn would return a response message to the cleint. The Nuclio Framwork contains functions which help to retrieve data from an HTTP request. This is handled via a separate `Request` class. The `Request` class is used in base classes such as: 

- `Controller`
- `Application`
- `Session`
- `Router` 

HTTP request data can therefore be easily accessed through any other class which inherits these base classes. 

For example, any `Controller` class can get a query string value by simply using the following:
```
    $request = $this->getRequest();
	$limit	= (int)$request->getQuery('limit');
```

<a name="request-class"></a>
## Request Class
In order to use `Request` class, you should create a new instance first, by using `getInstance` method.
```
$this->request = Request::getInstance();
```
<a name="request-method"></a>
### Request Method
The request method can be retrieved by using `getMethod` function.

<a name="request-query-string-value"></a>
#### Request Query String Value
Query string values can be retrieved by using `getQuery`. This method accepts a key and returns the value.
```
    $idQueryStringValue = $this->request->getQuery('Id');
```
> Note: In order to get the whole query string object, just pass nothing as the input.

<a name="validate-http-request-method"></a>
#### Validate HTTP Request Method
`isMethod` checks if the request uses same HTTP method as expected. 
```
    $isPOSTRequest = $this->request->isMethod('POST');
```
<a name="post-request-data"></a>
#### POST Request data
`getPost` returns the value from the `POST` request key value collection by accepting a key.

```
    $Name = $this->request->getPost('name');
```
> Note: In order to get the whole `POST` data, just pass nothing as the input.

<a name="put-request-data"></a>
#### PUT Request data
`getPut` returns the value from the `PUT` request key value collection by accepting a key.

```
    $Name = $this->request->getPut('name');
```
> Note: In order to get the whole `PUT` data, just pass nothing as the input.

<a name="head-request-data"></a>
#### HEAD Request data
`getHead`returns the the `HTTP header` value in a response object by accepting a key.

```
    $userAgentValue = $this->request->getHead('User-Agent');
```
> Note: In order to get the whole `head` object, just pass nothing as the input.

<a name="option-request-data"></a>
#### OPTION Request data
`getOption` returns the the communication options for the target resource.

```
    $serverDate = $this->request->getOption('Date');
```
> Note: In order to get the whole `option` object, just pass nothing as the input.

<a name="server-request-data"></a>
#### SERVER Request data
`getServer` returns the the server data.

```
    serverName = $this->request->getOption('SERVER_NAME');
```
> Note: In order to get the whole `server` object, just pass nothing as the input.

<a name="cookie-request-data"></a>
#### COOKIE Request data
`getCookie` returns the cookie data from the request.

```
    $cookie = $this->request->getCookie('SessionName');
```
> Note: In order to get the whole `cookie` object, just pass nothing as the input.

<a name="file-request-data"></a>
#### File Request data
`getFile` returns the uploaded file from the request.

```
    $uploadedPhoto = $this->request->getFile('photo');
```
> Note: In order to get the whole `uploaded file` object, just pass nothing as the input.

<a name="request-object"></a>
#### Request object
`getRequest` returns the request object.

```
    $currentRequest = $this->request->getRequest();
```
<a name="request-body-raw-data"></a>
#### Request Body Raw Data
`getRaw` returns the request body as raw data. The following is an example of reading `Json` value from a request body.
```
    $requestBody = $this->request->getRaw();
    $JSONReader=JSON::getInstance();
    $data = $JSONReader->decode($requestBody);
```
<a name="retrieve-response-header"></a>
#### Retrieve Response Header
`getHeaders` returns the response header object.

<a name="retrieve-values"></a>
#### Retrieve Values of Response Header
`getHeader` returns the response header value of a specific attribute.
```
    $contentType = $this->getHeader('Content-Type');
```
<a name="retrieve-language"></a>
#### Retrieve Language 
`getLanguages` returns 'accept-language' value from the header.

```
	$request->getLanguages();
```

<a name="retrieve-format"></a>
#### Retrieve Response Format
`getResponseFormats` returns 'accept' value from response header.

```
	$request->getResponseFormats();
```

<a name="request-secure"></a>
#### Check if Request is Secure
`isSecure` checks if the request is using a HTTPS protocol to communicate.
```
	$request->isSecure();
```

<a name="request-ajax"></a>
#### Check if Request is an Ajax
`isAjax` checks if the request is an `Ajax` call.

```
	$request->isAjax();
```

<a name="retrieve-server-name"></a>
#### Retrieve Server Name
`getDomain` returns the server name from `SERVER_NAME` attribute in request header.

```
	$request->getDomain();
```

<a name="retrieve-host-name"></a>
#### Retrieve HOST Name
`getHost` returns the target host from `HTTP_HOST` attribute in request header.

```
	$request->getHost();
```

<a name="responses"></a>
## Reponses

In the Nuclio Framework, there is no separate response class. Any responding functionality is built into the controller class or controller traits.