---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Nuclio Release Methodology
    # Desc of page, used for SEO, start of sentence super important
    description: How we release versions of the Nuclio Framework
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Release Methodology

- [Introduction](#intro)
- [Semantic Numbering](#numbering)
- [Core and Plugin Packaging](#packaging)
- [LTS vs Bleeding Edge](#stability)
- [LTS Support Duration](#support)
- [Logger Drivers](#logger-drivers)

<a name="intro"></a> 
##Introduction
The Nuclio Framework comprises of a set of core code, and many, many plugins. 
Each of these is developed in parallel, and periodically are bundled together
to form either a LTS release (with long term support) or a Bleeding Edge release.

This document describes the semantic versioning numbers we use, and how to tell
whether a release is LTS or Bleeding Edge.

<a name="numbering"></a>
##Semantic Numbering
We use numbers

<a name="packaging"></a>
##Packaging Core and Plugins
For each release, we will package together the latest Nuclio Core and the latest (or the latest stable, tested and compatible)
version of each of the plugins. We then give this collection a new semantic version number (see above)
and publish it as a release of the 'Nuclio Framework'

<a name="stability"></a>
##LTS vs Bleeding Edge
*Odd numbered releases are LTS releases
* Even numbered releases are Bleeding Edge releases

<a name="support"></a>
##LTS Support Duration
Free support is available for a LTS release for 2 years.
We aim to have a fresh LTS release at least every 12 months

