---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Error handling
    # Desc of page, used for SEO, start of sentence super important
    description: How to handle Errors within the Nuclio Framework
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
# Error Handling

In Nuclio, each plugin has its own specific class to handle the exceptions which are inherited from the `PluginException` class in `nuclio\core\plugin`.

Each plugin exception class like `AuthException` or `CacheException` is inherited from `NuclioException` which is the exception abstract class in Nuclio. 

For example, the `cache` plugin has a class called `CacheException`.

```
	use nuclio\exception\NuclioException;

	class CacheException extends NuclioException
	{

	}
```

Furthermore, each function in a plugin has its own exception type to throw. In the case of the authorisation plugin, if a user does not have access to the system, it throws an `AuthException`.
```
use nuclio\plugin\auth\AuthException;
public function authenticate()
{
    $user=User::findOne(Map{'email'=>$email});
		if(!is_null($user))
		{
		    // Authenticate user
		}
		else
		{
		    throw new AuthException('Invalid Login');
		}
}
```
