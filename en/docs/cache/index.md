---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Caching
    # Desc of page, used for SEO, start of sentence super important
    description: How application level caching works within Nuclio
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Cache
[Introduction](#introduction)
[Quick Tips](#quick_tips)
[Cache Handler examples](#handler)

<a name="introduction"> </a>
The Nuclio Framework provides a robust caching solution for applications which need to deal with large and unpredictable volumes of data. Enhance the performance of your application with our in-memory and key-value data store.

<a name="quick_tips"> </a>
##Quick Tips
Nuclio also supports other popular backend caching system such as [Memcached.](https://memcached.org/)

<a name="handler"> </a>
##Examples - Cache Handler
Cache handler is an interface class which will redirect the functions to the specified driver, if it exists.

`namespace nuclio\plugin\cache`


- `Store`
Stores data into the cache for $key.

An example of this is: 
```
    store(string $cacheKey, mixed $data, int $expiryTime = 0, ?string $folder=null):bool

```

- `Retrieve`
Retrieves data from cache. It returns NULL when no data is found.

An example of this is: 
```
    public function retrieve(string $cacheKey, ?string $subFolder=null):mixed
```

   
- `Clear`
Clears the cache. In the case of a file system cache, clears all cache files.


An example of this is:
```
    public function clear(?string $subFolder=null):bool
```

- `Free`
Removes the key from the cache.

An example of this is:
```
    public function free(string $cacheKey, ?string $subFolder=null):bool
```