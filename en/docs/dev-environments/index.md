---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Development Environments for Nuclio
    # Desc of page, used for SEO, start of sentence super important
    description: The different development environments available for the Nuclio Framework
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Development Environments
- [Introduction](#introduction)
- [Isolation Levels](#isolation)
- [Vagrant Virtual Machine](#vagrant)

<a name="introduction"></a>
##Introduction
A Development Environment is a great way to get started when learning a new language
or framework. They offer several benefits with only a couple of downsides. 

Benefits:
* They're a self contained/isolated environment, minimising changes required to your system
* You can run multiple, differing environments alongside each other.
* Any changes made to the environment don't affect anything else
* Access to the rest of your machine's resources is limited, so if your software goes haywire and tries to reformat your disk, only the dev environment is affected.
* They're temporary, when you're not developing, you can turn them off to free up system resources
* They're temporary, once you're done you can delete everything safely

Drawbacks:
* Running the environment takes some extra CPU power
* If you delete the environment, you loose everything in it (which is why you're using source control... right?)
* Mapping the connections between the environment and your local machine (disk, network etc etc) can be complicated

Depending on the level of isolation you require, there are different options.

<a name="isolation"></a>
##Isolation Levels
The isolation level, refers to how isolated the development environment is from your local machine. 
The environment doesn't just contain the framework you're working on, but will typically also contain
supporting software such as web servers and database servers.

In the case of the Nuclio Framework, a minimal development environment would consist of just the HHVM, Composer and the Nuclio Framework.
As you add extra services such as the webserver (nginx, Apache) and a datastore (MySQL, MongoDB) you have the choice
of whether you want to run these inside the environment, or have your dev environment use services already installed
on your local machine.

### Low Isolation
The lowest level of isolation, is a direct install of the framework onto your machine - effectively no isolation at all.
The framework, and your application has full access to your system, and you must rely on the permissions system of your OS to restrict access.
This combination requires the maximum number of changes to your machine, but achieves the maximum performance. 
Typically used for production servers or machines that are fully dedicated to developing with a particular target infrastructure in place, but configuration
is individual to each machine, and you have to deliberately take steps to avoid inconsistencies between machines.

Follow the [installation](../installing/index.html) instructions to achieve this.

### Medium Isolation
A _container_ achieves a medium level of isolation. Your application (in this case, the Nuclio Framework) is isolated from the host system, but relies on the underlying operating system; multiple containers on the same physical machine share the operating system. This reduces the overhead of having to provide their own operating system, but you're unable to have incompatible operating systems or configuration setups on the same machine.
Containers are designed to be transient, so their creation and destruction is part of their life cycle and tends to be scripted.

We plan to provide a Docker container preconfigured with the Nuclio Framework in the near future.

### High Isolation
A _virtual machine_ provides the highest level of isolation. A full machine with an independent
operating system is 'simulated', allowing the highest level of control over the environment. 
This allows you to run incompatible operating systems on the same physical machine (such as an Ubuntu
virtual machine on a Windows host). In many cases, the software in the virtual machine won't even know that it's
running in a simulated environment.

All this simulating tends to take additional CPU and memory resources, so smaller, underpowered 
host machines may struggle under the load. 

<a name="vagrant"></a>
We have a [Vagrant VM installation](../dev-environment-vagrant/index.html) that you can use.

### Summary
From left to right, the lowest to the highest levels of isolation:
![Isolation Levels](./isolation_levels.png)
