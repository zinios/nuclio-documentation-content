---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Other Useful 3rd Party Software Applications
    # Desc of page, used for SEO, start of sentence super important
    description: Useful 3rd Party Software Applications which can be install for good hacking
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"


#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Useful 3rd Party Software Applications
- [Introduction](#introduction)
	- [Atom](#atom)
		- [Nuclide](#nuclide)
	- [PuTTY](#putty)
	- [Ubuntu 16.04](#ubuntu)
    - [Vagrant](#vagrant)
    - [HHVM](#hhvm)
    - [VirtualBox](#virtualbox)
    - [Git](#git)
    - [libtool](#libtool)

<a name="introduction"> </a>
##Introduction

Although not mandatory, there a several small software applications available which will aid in the development of your apps. We have found the following applications very useful and hope you will too.

<a name="atom"> </a>
###Atom
A useful text editor which supports hacklang. It can be downloaded [here.](https://atom.io/)

<a name="nuclide"> </a>
####Nuclide
Nuclide is the first IDE with support for Hack, including autocomplete, jump-to-definition, inline errors, and an omni-search bar for your project. This app goes on top of your *Atom* install for additional hacking support. It can be downloaded [here.](https://nuclide.io/docs/quick-start/getting-started/#installation)

<a name="putty"> </a>
###PuTTY (for windows user)
PuTTY is an SSH and telnet client for windows. 

NOTE: mac and linux users can use the command line for these features. 

####How to install Putty

1. Download from [website](http://www.putty.org/).
2. Save executable file in a known location. e.g. c:\
3. Double click to run


<a name="ubuntu"> </a>
### Ubuntu 16.04

Ubuntu is an open source Linux desktop and server operating system.

Ubuntu 16.04 can be downloaded here for [server](https://www.ubuntu.com/download/server) or [desktop](https://www.ubuntu.com/download/desktop)

<a name="vagrant"> </a>
### Vagrant 

Vagrant is used to create and configure lightweight, reproducible, and portable development environments.

It is recommended to use  1.8.5 or above.

Vagrant can be downloaded [here.](https://www.vagrantup.com/downloads.html)

<a name="hhvm"> </a>
### HHVM 

HipHop Virtual Machine (HHVM) is an open-source virtual machine designed for executing programs written in Hack and PHP. It is recommended to use version 3.15 (LTS).
> Note: HHVM currently only fully supports Linux OS.

HHVM can be downloaded [here.](https://docs.hhvm.com/hhvm/installation/introduction)

To install HHVM on Ubuntu 16.04, open up the terminal and run:
``` 
sudo apt-get install hhvm
```


To install HHVM on Ubuntu 14.04 Trusty64, open up the terminal and run the following 5 lines:
```
// installs add-apt-repository
sudo apt-get install software-properties-common

sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449
sudo add-apt-repository "deb http://dl.hhvm.com/ubuntu $(lsb_release -sc) main"
sudo apt-get update
sudo apt-get install hhvm
```

<a name="virtualbox"> </a>
### VirtualBox 

VirtualBox is a cross-platform virtualization application. It is recommended to use version 5.1.4 or above.

VitrualBox can be downloaded [here.](https://www.virtualbox.org/manual/ch01.html#intro-installing)

<a name="git"> </a>
### Git 

Git is a free and open source distributed version control system . 

The following command in terminal will install GIt on Ubuntu:

`$ sudo apt-get install git-all`

More information about GIT can be found [here.](https://git-scm.com/downloads)

<a name="libtool"> </a>
### libtool

Libtool is a generic library support script. 

Libtool can be downloaded [here.](https://www.gnu.org/software/libtool/)