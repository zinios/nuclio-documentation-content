---
# html level variables used to create the page, good for SEO, social media etc
html:
    # The directory depth, or how many '../' are required to get back to root
    # !!IMPORTANT!! of this version of the documentation - not the root of the domain
    pathToRoot: ../../
    # Title of the page, good for SEO
    title: System Requirements
    # Desc of page, used for SEO, start of sentence super important
    description: Requirements to install and run the Nuclio Framework
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#System Requirements

The Nuclio Framework itself requires:
1 An installed and working version of [Composer](http://getcomposer.org) (In order to download the framework, it's plugins and it's dependencies 
1 [HHVM](http://hhvm.com)


It is highly recommended that you use a Web server to handle incoming requests such as [Apache](https://httpd.apache.org/) or [Nginx](https://www.nginx.com/). You may also use the built-in HHVM [Proxygen](https://docs.hhvm.com/hhvm/basic-usage/proxygen), a built-in webserver used by Facebook.

A data storage engine such as [MongoDB](https://www.mongodb.com/) or [MySQL](http://www.mysql.com/) is also recommended.

