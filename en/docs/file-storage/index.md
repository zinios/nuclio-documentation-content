---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: File Storage
    # Desc of page, used for SEO, start of sentence super important
    description: How to read and write files, and manipulate directories.
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#File System
- [Introduction](#introduction)
- [Drivers](#drivers)
	- [Files and Folders functions using path](#FilesFoldersPath)
	- [Folders](#folders)
	- [File Reader](#reader)


<a name=“introduction”> </a>
The Nuclio Framework includes a robust file system which provide straight forward drivers for working with local file systems, Amazon S3 etc.
The Nuclio file system gives you a convenient way for using server hard disk space as the main storage for the system.

`namespace nuclio\plugin\fileSystem`

<a name=“drivers”> </a>
##Drivers
Nuclio supports numerous types of file systems, as listed below:

 - Local disk
 - Memory
 - Amazon S3

 Almost all the drivers have the same functionality except for the Amazon S3 it will change a bit to work perfectly with AWS framework.

<a name=“FilesFoldersPath”> </a>
##Files and Folders functions using path
###Checking if a file exists
The `exist` function checks if a file exists at the provided path.

An example of this is:
```
    public function exists(string $path):bool
    {
        return file_exists($path);
    }
```
###Reading contents of a file
The `read` function is used to read the contents of a file, given a path.

An example of this is:
```
    public function read(string $path):string
    {
        if($this->isFile($path))
        {
            $content=file_get_contents($path);
            return $content;
        }
        else
        {
            return 'Not a file!';
        }
    }
```
### Creating or overwriting a file
The `write` function is used for creating a file that does not exist, or overwriting an existing file, at a specific path.

An example of this is:
```
    public function write(string $path, string $contents):bool
    {
        return (bool)file_put_contents($path, $contents);
    }
```
###Appending contents to a file
The `append` function is used for appending contents into a file at a specific path.

An example of this is:
```
    public function append(string $path, string $contents):bool
    {
        return (bool)file_put_contents($path, $contents, FILE_APPEND);
    }
```
###Creating an empty file (touch)
The `touch` function is used for creating an empty file by using the touch function, with path provided.

An example of this is:
```
    public function touch(string $path):bool
    {
        return (bool)touch($path);
    }
```
###Deleting a file
The `delete` function is used to delete a file at the provided path.

An example of this is:
```
    public function delete(string $path, int $rules):bool
    {
        if ($this->isFile($path))
        {
            if (unlink($path))
            {
                return true;
            }
            throw new LocalDiskException('Something happen, cant delete file.');
        }
        throw new LocalDiskException('File does not Exist!');
    }
```
###Moving a file
The `move` function is used to move a file from the source path to the destination path.

An example of this is:
```
    public function move(string $path, string $target, int $rules):bool
    {
        return rename($path, $target);
    }
```
###Copying a file
The `copy` function is used to copy a file from the source path to the destination path.

An example of this is:
```
    public function copy(string $path, string $target):bool
    {
        return copy($path, $target);
    }
```
###Obtaining the file name
The `getname` function is used to obtain the file name.

An example of this is:
```
    public function getName(string $path):?string
    {
        return pathinfo($path, PATHINFO_FILENAME);
    }
```
###Obtaining a file extension
The `getExt` function is used to obtain the file extension.

An example of this is:
```
    public function getExt(string $path):?string
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }
```
###Obtaining the file type
The `getType` function is used to obtain the file type.

An example of this is:
```
    public function getType(string $path):?string
    {
        return filetype($path);
    }
```
###Obtaining the file size
The `getSize` function is used to obtain the file size.

An example of this is:
```
    public function getSize(string $path):int
    {
        return filesize($path);
    }
```
###Checking if a parameter given is a directory
The `isDirectory` function is used to check if a parameter given is a directory.

An example of this is:
```
    public function isDirectory(string $directory):bool
    {
        return is_dir($directory);
    }
```
###Checking if a file is empty
The `isEmpty` function is used to check if a file is empty.

An example of this is:
```
    public function isEmpty(string $directory):bool
    {
        return (count(scandir($directory)) == 2);
    }
```
###Checking if the given path is writable
The `isWritable` function is used to check if a path is writable.

An example of this is:
```
    public function isWritable(string $path):bool
    {
        return is_writable($path);
    }
```
###Listing all files and folders in a directory
The `listAllFilesAndFolders` function is used to list all the files and folders in a directory.

An example of this is:
```
    public function listAllFilesAndFolders(string $directory):Vector<string>
    {
        return new Vector(scandir($directory));
    }
```
###Creating a directory
The `create` function is used to create a directory.

An example of this is:
```
    public function create(string $path, int $mode = 0755, bool $recursive = false):bool
    {
        return mkdir($path, $mode, $recursive);
    }
```
###Checking of the directory is readable
The `isReadable` function is used to check if the directory is readable.

An example of this is:
```
    public function isReadable(string $path):bool
    {
        return is_readable($path);
    }
```
###Looking for files and folders in a specific path
The `find` function is used to list the files and folders in a specific path.

An example of this is:
```
    public function find(string $path, string $regex):Vector<string>
    {
        return new Vector(null);
    }
```
###Deleting files in a directory
The `deleteFilesInDir` function is used to delete the file(s) in a directory.

An example of this is:
```
        public function deleteFilesInDir(string $directory):bool
        {
            if (!$this->isDirectory($directory)) 
            {
                return false;   
            }
        
            $files = glob($directory.'/*');
            foreach($files as $file)
            {
                if($this->isFile($file))
                {
                    unlink($file);
                }
            }
          
    
      return true;
    }
```
<a name=“folders”></a>
##Folders
This section describes the functions to handle folders.

###Constructor
The `constructor` instantiates the requested driver and goes to the current folder location. It also stores a list of the subdirectories in the current folder.

An example of this is:
```
    public function __construct(string $driver, string $path)
    {
        parent::__construct();
        
        $this->driverName = $driver;
        $tmpDriver=ProviderManager::request($driver);
        if($tmpDriver instanceof CommonInterface)
        {
            $this->driver = $tmpDriver;
        }
        else
        {
            throw new FolderException(sprintf('No driver found for "%s".',$driver));
        }
        if(!$this->getDriver()->isDirectory($path))
        {
            throw new FolderException(sprintf('"%s" Is not a folder',$path));
        }
        $this->location =   $path;
        $this->cursor   =   0;
        $this->list     =   $this->getDriver()->listAllFilesAndFolders($this->location);
    }
```
###Getting a count of the items inside a folder
The `count` function obtains a count of the items inside a folder.

An example of this is:
```
    public function count():int
    {
        return count($this->list);
    }
```
###Getting the information of the current folder
The `current` function returns the information of the current folder.

An example of this is:
```
    public function current():mixed
    {
        return $this->current;
    }
```
###Getting the order of the current folder
The `key` function returns the the order of the current folder in the list.

An example of this is:
```
    public function key():int
    {
        return $this->cursor;
    }
```
###Moving to the next folder
The `next` function enable you to move to the next folder in the list.

An example of this is:
```
    public function next():void
    {
        ++$this->cursor;
        $this->setCurrent();
    }
```
###Checking the cursor value
The valid function enables you to check if the cursor value is in the list of the files in the current folder.

An example of this is:
```
    public function valid():bool
    {
        return ($this->cursor < $this->count());
    }
```
###Getting the folder's path
The `getPath` function obtain the folder's path.

An example of this is:
```
    public function getPath():string
    {
        return $this->list[$this->key()];   
    }
```
###Getting the folder's full path
The `getFullPath` function obtain the current folder full path.

An example of this is:
```
    public function getFullPath():string
    {
        return $this->getLocation() . _DS_ . $this->list[$this->key()];
    }
```
###Checking the dots of the current element
The `isDot` function checks weather the current element is '.' or '...'.

An example of this is:
```
    public function isDot():bool
    {
        return ($this->list[$this->key()]=='.' || $this->list[$this->key()]=='..');
    }
```
<a name=“reader”> </a>
##File Reader
This section provides functions to handle files.

###Reading contents of a file
The `read` function reads the contents of a file.

An example of this is:
```
    public function read():string
    {
        if(!$this->isReadable())
        {
            throw new FileReaderException("Not readable!");
        }   
        return $this->getDriver()->read($this->file);
    }
```
###Getting the file name
The `getName` functions returns the file name of a particular file.

An example of this is:
```
    public function getName():?string
    {
        return $this->getDriver()->getName($this->file);
    }
```
###Getting the file extension
The `getExt` function returns the file extension of a particular file.

An example of this is:
```
    public function getExt():?string
    
        {
            return $this->getDriver()->getExt($this->file);
        }

```
###Getting the file type
The `getType` function returns the file type of a particular file.

An example of this is:
```
    public function getType():?string
    {
        return $this->getDriver()->getType($this->file);
    }
```
###Getting the file size
The `getSize` function returns the file size of a particular file.

An example of this is:
```
    public function getSize():?int
    {
        return $this->getDriver()->getSize($this->file);
    }
```