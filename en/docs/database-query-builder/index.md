---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Query Builder
    # Desc of page, used for SEO, start of sentence super important
    description: How to use the Query Builder.
    # subtitle of the page
    subtitle:
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"


#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---


<a name="model"></a>
# Query Builder

- [Query Builder](#queryBuilder)
  - [Relationships](#relationships)


A big reason why Nuclio can remain compatible between different database servers is the  `Query Builder`.

The query builder comes with drivers which rewrite queries to the their native formats.

Currently Nuclio's Query Builder supports the following databases:

* MongoDB
* MySQL

Queries are written in mongo-like syntax, using Maps and Vectors (You may also use arrays as the shorthand can look better if your query is rather verbose).

```
use nuclio\plugin\database\
{
  datasource\manager\Manager,
  queryBuilder\QueryBuilder
}
use nuclio\plugin\user\model\User;

$builder=QueryBuilder::getInstance(Manager::getSource('primary'));

$query	=$builder->setTarget(User::class)
                  ->setFilter
                  (
                    Map
                    {
                      '$or'=>
                      [
                        [
                          'email'		=>'foo@bar.com',
                          'password'	=>'p@ssw0rd'
                        ],
                        [
                          'status'	=>1
                        ]
                      ]
                    }
                  )
                  ->setLimit(10)
                  ->setOffset(0);
              $cursor=$builder->execute();
foreach ($cursor as $record)
{
  var_dump($record->toMap());
}
```

You can use *most* MongoDB query syntax. Check the MongoDB documentation for further details and post a bug report if there is something which is not supported.


## Relationships

A special note on relationships.

While we have built the query builder to work between relational and non-relational databases, joining between tables/collections is not yet supported. Future versions of the Query Builder will support this functionality.
