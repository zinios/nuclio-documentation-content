---
# html level variables used to create the page, good for SEO, social media etc
html:
    # Title of the page, good for SEO
    title: Nuclio Installation
    # Desc of page, used for SEO, start of sentence super important
    description: How to install the Nuclio Framework
    # subtitle of the page
    subtitle: 
    #folder depth - how many folders deep is this file - IN THE FINAL LAYOUT (not local preview)
    folderDepth: 4

#The version of nuclio that this document refers to
nuclioVersion: "1.0"

#Properties of the sidebar
sidebar:
    #Allows for different sidebars for each release
    version: 1

#for multilingual support
language:
    #the collection of languages supported on this page
    collection: 1
    #the language this page is written in
    current: en

---
#Quick Start Guide
- [Prerequisites ](#prerequisites)
- [Installation](#install)
- [Development Environment](#vagrant)
	
<a name="prerequisites"></a>
##Prerequisites 
The following prerequisites are required to be installed before you can run your Nuclio setup for a production environment:

- Ubuntu Trusty 64
- Terminal  
- MongoDB or MySQL
- HHVM - 3.15 (LTS)
- Composer
- Git 

Installation procedures for the various recommended 3rd party software can be found [here.](../3rdparty/index.html)
> Note: This guide assumes you have an Atlassian Bitbucket account.

<a name="install"></a>
## Installation:

If you haven't already done so, install [Composer](http://getcomposer.org) and [HHVM](http://hhvm.com)

In your terminal window
1. `git clone https://bitbucket.org/zinios/nuclio-standard.git ./nuclio/`
1. `cd nuclio` and then `composer install` 
1. Check that your webserver user:group has access to the directory: `sudo chmod -R 777 .` is a temporary workaround to get you started quickly
1. Create a new project with basic configuration.
>`$ hhvm vendor/bin/atomos make::app --name MyFirstApp --sample`

If you don't want sample code to be included, remove the "--sample" argument.
8. Run nuclio setup 
> `$ sudo hhvm vendor/bin/nuclio setup --config MyFirstApp/config/`



<a name="vagrant"></a>
## Development Environment 

If you are looking for a self-contained development, for ease of use and because we know you just want to start hacking, we have created a Nuclio Vagrant file to start off your development. More information about our vagrant environment can be found [here.](../dev-environment-vagrant/index.html)

